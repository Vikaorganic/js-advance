/*

    Задание 1:
    Написать скрипт, который по клику на кнопку рандомит цвет и записывает его в localStorage
    После перезагрузки страницы, цвет должен сохранится.

*/

function getRandomIntInclusive (min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function setColor(){
    var rColor = getRandomIntInclusive(0, 255);
    var gColor = getRandomIntInclusive(0, 255);
    var bColor = getRandomIntInclusive(0,255);

    let color = 'rgb(' + rColor + ',' + gColor + ',' + bColor + ')';

    localStorage.setItem('bgColor', color);

    document.body.style.background = color;
}

let btn = document.createElement('button');

btn.innerText = 'set random color';

btn.addEventListener('click', setColor);

document.body.appendChild(btn);

let colorFromLocalStorage = localStorage.getItem('bgColor');

if(colorFromLocalStorage !== null) {
    document.body.style.background = colorFromLocalStorage;
}