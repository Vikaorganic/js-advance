/*

    Задание 3:

    Написать форму с 2 полями для title и about
    при сабмите формы должен создаватся пост на основе класа Post
    <form >     
        <input type="text" name="title">
        <textarea name="about"></textarea>
        <button>Send</button>
    </form>

    Написать класс Post в котором есть данные:

    _id    
    title,
    body,
    likes

    У класса должен быть метод addLike и render.

    Нужно сделать так чтобы:
    - После добавления поста, данные о нем записываются в localStorage.
    - После перезагрузки страницы, данные должны сохраниться.
    - Можно было предзагрузить данные в класс из апи: https://jsonplaceholder.typicode.com/posts


*/

let posts = [];

const onFormSubmit = (e) => {
    e.preventDefault();

    let newPost = new Post({title: e.target.title.value, body: e.target.about.value});
    posts.push(newPost);
    localStorage.setItem('posts', JSON.stringify(posts));
}

let mainForm = document.createElement('form');

mainForm.innerHTML = `
    <div class="container border border-success border-1 rounded p-4 mt-4">
        <div class="mb-3">
            <label for="formTitle" class="form-label">Title</label>    
            <input type="text" class="form-control" id="formTitle" name="title" placeholder="add title">
        </div>
        <div class="mb-3">
            <label for="formAbout" class="form-label">About</label>    
            <textarea class="form-control" id="formAbout" name="about" placeholder="type your text here" rows="3"></textarea>
        </div>
        <button type="submit" class="btn btn-primary">Create post</button>
    </div>
`;

mainForm.addEventListener('submit', onFormSubmit);

document.body.appendChild(mainForm);

//container for already created posts
let postsContainer = document.createElement('div');
postsContainer.className = 'container border border-success border-1 rounded p-4 mt-4';
postsContainer.innerHTML = '<h2 class="text-center mb-4">Posts will be rendered below</h2>';

document.body.appendChild(postsContainer);

class Post {
    constructor({id = null, title, body, likes = 0}) {
      this._id = id || new Date().getTime();
      this.title = title;
      this.body = body;
      this.likes = likes;

      this.render();
    }
    get id() {
        return this._id;
    }

    addLike = () => {
        this.likes++;
    }

    render = () => {
      let elem = document.createElement('div');
      elem.className = 'card mb-4';
      elem.innerHTML = `
        <div data-id="${this.id}" class="card-body">
            <h5 class="card-title">${this.title}</h5>
            <p class="card-text">${this.body}</p>
            <button type="button" class="btn btn-primary add_like">Add like</button>
            <span class='likes'>Likes ${this.likes}</span>
        </div>
        `;

      postsContainer.appendChild(elem);
    }
}

let postsCallback = () => {
    Array.from(document.querySelectorAll(".card-body .btn")).forEach(function(element) {
    element.addEventListener('click', (e)=>{
      let card = e.currentTarget.closest('.card-body');    
      let likes = e.currentTarget.closest('.card-body').querySelector('span');
      console.log(posts);
      likes.innerHTML =  td.getAttribute("data-content");
    });
  });
};

let postsFromStorage = localStorage.getItem('posts');

if (postsFromStorage !== null) {
    posts = JSON.parse(postsFromStorage);
    posts.forEach(item => new Post({id: item._id, title: item.title, body: item.body, likes: item.likes}));
}

document.body.addEventListener('click',function(e){
    if(e.target && e.target.classList.contains('add_like')){
        let card = e.target.closest('.card-body');    
        let likes = e.target.closest('.card-body').querySelector('span');
        let currentPost = posts.find(obj => {return obj._id == card.dataset.id})
        currentPost.likes += 1;
        localStorage.setItem('posts', JSON.stringify(posts)) // update posts collection with new likes count value
        likes.innerHTML =  `Likes ${currentPost.likes}`;
     }
 });