/*

    Задание 2:
    Написать форму логина (логин пароль), которая после отправки данных записывает их в localStorage.
    Если в localStorage есть записть - На страниче вывести "Привет {username}", если нет - вывести окно
    логина.

    + бонус, сделать кнопку "выйти" которая удаляет запись из localStorage и снова показывает форму логина
    + бонус сделать проверку логина и пароля на конкретную запись. Т.е. залогинит пользователя если
    он введет только admin@example.com и пароль 12345678


*/

// user form code
let form = document.createElement('form');

form.innerHTML = `
    <input type="text" name="userName" />
    <input type="password" name="userPass" />
    <button type="submit">submit</button>
`;

// user greetings code
let userGreetingMessage = document.createElement('p');

const onFormSubmit = (e) => {
    e.preventDefault();

    let userName = form.userName.value;
    let userPass = form.userPass.value;

    let userData = {
        userName,
        userPass
    };

    let jsonUserData = JSON.stringify(userData);

    console.log(userData);

    localStorage.setItem('userData', jsonUserData);

    location.reload();
}

form.addEventListener('submit', onFormSubmit);

let dataFromLocalStorage = localStorage.getItem('userData');

if (dataFromLocalStorage !== null) {
    console.log(dataFromLocalStorage);

    let lsObj = JSON.parse(dataFromLocalStorage);

    userGreetingMessage.innerText = `Привет ${lsObj.userName}`;

    document.body.appendChild(userGreetingMessage);

    let close = document.createElement('button');

    close.classList.add('logOut');

    close.innerHTML = 'exit';

    document.body.appendChild(close);
} else {
    document.body.appendChild(form);
}

document.querySelector('button.logOut').addEventListener('click', function(e) {
    localStorage.removeItem("userData");

    location.reload();
})
