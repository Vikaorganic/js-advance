/*

  Задание:

    Написать при помощи async-await скрипт, который:

    Получает список пользователей:  https://jsonplaceholder.typicode.com/users
    Перебирает, выводит табличку:

    # | userName | email           | Показать webiste   | Показать phone |
    1.| userName | email@test.com  | button             | button
    2.| userName | email@test.com  | test12.com         | button
    3.| userName | email@test.com  | button             | button
    4.| userName | email@test.com  | button             | button

    Данные о сайте и номер телефона скрывать при выводе и показывать при клике.

*/
window.addEventListener("load", function () {

  function getData(url){
    return fetch(url).then((response) => {
        return response.json();
      });    
  }

  async function makeTable(){
    let data = await getData('https://jsonplaceholder.typicode.com/users');
    data.forEach(user => {
      addUserRow(user);
      console.log(user)
      
    });
  }

  function addUserRow(user){
    let tbody = document.querySelector('#myTable tbody');
    tbody.innerHTML += `
    <tr>
    <td>${user.id}</td>
    <td>${user.username}</td>
    <td>${user.email}</td>
    <td data-content='${user.website}'><button class='show'>Show website</button></td>
    <td data-content='${user.phone}'><button class='show'>Show phone</button></td>
    </tr>
    `
  }

  makeTable().then(()=>{
    Array.from(document.querySelectorAll(".show")).forEach(function(element) {
      element.addEventListener('click', (e)=>{
        let td = e.currentTarget.closest('td');
        td.innerHTML =  td.getAttribute("data-content");
      });
    });
  });

});
