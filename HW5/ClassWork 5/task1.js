/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/

var train = {
    name: 'Киев - Херсон',
    speed: '90км.час',
    passNum: 200,
    go:function() {
        console.log(`Поезд ${this.name} везет ${this.passNum} пассажиров со скоростью ${this.speed}`);
    },
    stand: function() {
        console.log(`Поезд ${this.name} остановился. Скорость ${speed}`);
    },
    getPassanger: function(i) {
        return this.passNum += i
    }
}
