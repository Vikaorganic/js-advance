/*

  Задание:

    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);

    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }
      + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
      + В прототипе должен быть метод который увеличивает счетик лайков

    var myComment1 = new Comment(...);

    2. Создать массив из 4х комментариев.
    var CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.

    <div id="CommentsFeed"></div>


    
*/
window.onload = function() { 

function Comment (name, text, avatarUrl) {
  this.name = name;
  this.text = text;
  this.avatarUrl = avatarUrl;

  this.likesCount = 0;
} 

Comment.prototype.increaseCounter = function() {
  this.likesCount += 1;
}

var myComment1 = new Comment('Vasja', 'Hi', 'images/1.jpg');
var myComment2 = new Comment('Petja', 'Bye', 'images/2.jpg');
var myComment3 = new Comment('Masha', 'See you', 'images/3.jpg');
var myComment4 = new Comment('Katja', 'Hello', 'images/4.jpg');

var CommentsArray = [
  myComment1,
  myComment2,
  myComment3,
  myComment4
];

function renderCommentsTree(cmntsArr) {
  cmntsArr.map((comment, index) => {
    renderComment(comment, index);
  });

}

function renderComment(comment, index) {
  document.getElementById('CommentsFeed').innerHTML += `
  <div class="comment" data-id = ${index}>
    <div class="msg">${comment.text}</div>
    <div class = "avatar">
      <img src="${comment.avatarUrl}"/>
    </div>  
    <div class = "name">${comment.name}</div>
    <div class = "likes">${comment.likesCount}</div>

  </div>`
}

renderCommentsTree(CommentsArray);

document.querySelectorAll('.avatar img').forEach(element => {
 
  element.addEventListener('click', function(event) {
    var commentDiv = event.currentTarget.closest('.comment');
    
    CommentsArray[commentDiv.dataset.id].increaseCounter();
    var counter = parseInt(commentDiv.querySelector('.likes').innerHTML);
    commentDiv.querySelector('.likes').innerHTML = counter+1;
  })
})

}